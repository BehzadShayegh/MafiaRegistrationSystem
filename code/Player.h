#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include "myExeptions.h"

class Player {

public:
	enum Role {
		JOKER,
		SIMPLE_VILLAGER,
		SIMPLE_MAFIA,
		DETECTIVE,
		DOCTOR,
		ROUINTAN,
		GODFATHER,
		SILENCER
	};
	enum Team {
		SINGLE_JOKER,
		VILLAGER,
		MAFIA
	};

	Player(std::string);
	~Player();

	void go_to_silence();
	void go_to_heal();
	bool get_alive_status();
	int get_incoming_votes();
	bool get_healed_status();
	std::string get_username();
	void voting(Player*);
	void incoming_votes_inc();
	bool same_name(std::string);
	bool same_player(Player*);
	void apply_vote();
	bool made_vote();
	void clean_vote();
	void silencing_recovery();

	virtual void make_vote(Player*, bool);
	virtual void die(bool);
	virtual void recovery();

	virtual bool get_this_night_detected();
	virtual bool get_this_night_healed();
	virtual bool get_this_night_silence();
	virtual bool detect(Player*);
	virtual void save(Player*);
	virtual void make_silence(Player*);

	virtual bool is_mafia() = 0;
	virtual Role get_role() = 0;
	virtual Team get_team() = 0;
	
private:
	std::string name;
	bool healed;
	bool alive;
	bool silence;
	Player* vote_target;
	int incoming_votes;
};

#endif