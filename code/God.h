#ifndef GOD_H
#define GOD_H

#include <iostream>
#include <vector>
#include "Room.h"

class God {

public:
	God();
	void add_room(Room*);
	void switch_room(std::string);
	void join_player_to_this_room(std::string);
	bool expected_command(std::string);
	void vote(std::string, std::string);
	void finish_day();
	void detect(std::string,std::string);
	void heal(std::string,std::string);
	void silent(std::string,std::string);
	void finish_night_if_have_to();
	void get_room_state();
	void list_players();
private:
	bool is_night_finish();
	bool exist_room(Room*);

	std::vector<Room*> rooms;
	Room* running_room;

};

#endif