#ifndef MAFIA_H
#define MAFIA_H

#include "Player.h"

class Mafia : public Player {

public:
	Mafia(std::string);
	~Mafia();

	void make_vote(Player*, bool);
	
	bool is_mafia();
	Player::Role get_role();
	Player::Team get_team();
};
#endif