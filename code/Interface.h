#ifndef INTERFACE_H
#define INTERFACE_H

#include <iostream>
#include <vector>
#include <bits/stdc++.h>
#include "God.h"

class Interface {

public:
	enum Command{
		Create_room,
		Switch_room,
		Join,
		Vote,
		End_vote,
		Detect,
		Heal,
		Silent
	};

	Interface();
	void run();
	
private:
	void in_try(std::istringstream&);
	void create_room(std::istringstream&);
	void switch_room(std::istringstream&);
	void join(std::istringstream&);
	void vote(std::istringstream&);
	void end_vote();
	void detect(std::istringstream&);
	void heal(std::istringstream&);
	void silent(std::istringstream&);
	void read_roles(std::istringstream&, std::vector<Player::Role>&);
	void clean_same_roles(Player::Role, std::vector<Player::Role>&);
	void check_roles(std::vector<Player::Role>&);
	void clean_roles(std::vector<Player::Role>&, int, int);
	Command command_reader(std::string);
	Player::Role role_reader(std::string);


	God god;
};

#endif