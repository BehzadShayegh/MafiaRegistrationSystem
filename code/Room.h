#ifndef ROOM_H
#define ROOM_H

#include <iostream>
#include <vector>
#include <cstdlib>
#include "Player.h"
#include "Villager.h"
#include "Mafia.h"
#include "Joker.h"
#include "Detective.h"
#include "Doctor.h"
#include "RouinTan.h"
#include "GodFather.h"
#include "Silencer.h"

class Room {

public:
	Room(std::string, std::vector<Player::Role>);
	~Room();
	void join_player(std::string);
	bool same_room_name(std::string);
	bool same_room(Room*);
	bool next_time();
	std::string command_need();
	bool get_night_time();
	bool enough_player();
	void list_players();
	void vote(std::string,std::string);
	void detect(std::string,std::string);
	void heal(std::string,std::string);
	void silent(std::string,std::string);
	void print_state();
	bool get_ready_list_show();

private:
	Player::Role random_role();
	void calculate_team_numbers(Player::Role);
	void add_player(std::string);
	bool mafia_win();
	bool villager_win();
	bool end_game();
	void apply_all_night_votes();
	void apply_all_day_votes();
	int max_incoming_vote_index();
	void show_kill_trying(Player*);
	void show_kill_result(Player*);
	int kill_night_target();
	void kill_day_target();
	void end_night();
	void end_day();
	void show_night_flag();
	void show_day_flag();
	void show_die_result(Player*);
	void show_game_result();
	void check_not_repetitive_player(std::string);
	bool need_night_heal();
	bool need_night_silent();
	bool need_night_detect();
	bool need_night_vote();
	int find_player_index(std::string);
	bool is_this_index_alive(int);
	void select_night_target();
	void recovery_all();
	void recovery_all_silenced();
	void show_silences_result();
	void sort_indexes();
	void recovery_roles();
	std::string string_form_of_role(Player::Role);

	
	std::string name;
	std::vector<Player::Role> roles;
	std::vector<Player*> players;
	int number_of_mafias, number_of_villagers;
	bool joker_win;
	bool night_time;
	int night_target_index;
	std::vector<int> night_silenced_indexs;
	int day_number;
	bool ready_to_show_list;
};

#endif