#include "Room.h"
using namespace std;

Room::Room(string _name , vector<Player::Role> _roles) : name(_name) {
	night_time = false;
	joker_win = false;
	day_number = 1;
	number_of_mafias = 0;
	number_of_villagers = 0;
	night_target_index = -1;
	ready_to_show_list = false;

	for(int i=0; i<_roles.size(); i++){
		roles.push_back(_roles[i]);
		calculate_team_numbers(_roles[i]);
	}
}

Room::~Room() {
	for(int i=0; i<players.size(); i++)
		delete players[i];
}

bool Room::get_ready_list_show() { return ready_to_show_list; }

void Room::calculate_team_numbers(Player::Role role) {
	switch(role){
		case Player::SIMPLE_VILLAGER:
		case Player::DETECTIVE:
		case Player::DOCTOR:
		case Player::ROUINTAN:
			number_of_villagers++;
			break;
		case Player::SIMPLE_MAFIA:
		case Player::GODFATHER:
		case Player::SILENCER:
			number_of_mafias++;
			break;
	}
}

bool Room::same_room(Room* other){ return this->name == other->name; }

Player::Role Room::random_role() {
	int random_index = rand() % roles.size();
	Player::Role role = roles[random_index];
	roles.erase(roles.begin() + random_index);
	return role;
}

void Room::add_player(string _name) {
	switch(random_role()) {
		case Player::JOKER:
			players.push_back(new Joker(_name));
			break;
		case Player::SIMPLE_VILLAGER:
			players.push_back(new Villager(_name));
			break;
		case Player::SIMPLE_MAFIA:
			players.push_back(new Mafia(_name));
			break;
		case Player::DETECTIVE:
			players.push_back(new Detective(_name));
			break;
		case Player::DOCTOR:
			players.push_back(new Doctor(_name));
			break;
		case Player::ROUINTAN:
			players.push_back(new RouinTan(_name));
			break;
		case Player::GODFATHER:
			players.push_back(new GodFather(_name));
			break;
		case Player::SILENCER:
			players.push_back(new Silencer(_name));
			break;
	}
}

void Room::check_not_repetitive_player(string _name) {
	for(int i=0; i<players.size(); i++)
		if(players[i]->same_name(_name))
			throw new RepetitivePlayer();
}

void Room::join_player(string _name) {
	if(roles.size() > 0){
		check_not_repetitive_player(_name);
		add_player(_name);
		if(roles.size()==0) ready_to_show_list = true;
	}
	else {
		ready_to_show_list = false;
		recovery_roles();
		throw new _ManyUsers_();
	}
}

void Room::recovery_roles() {
	while(players.size() > 0){
		roles.push_back(players[players.size()-1]->get_role());
		delete players[players.size()-1];
		players.pop_back();
	}
}

bool Room::enough_player() { return roles.size() == 0; }

bool Room::same_room_name(string _name) { return _name == name; }

bool Room::end_game() { return joker_win || mafia_win() || villager_win(); }

void Room::show_game_result() {
	if(joker_win)
		cout << "Do I look like a guy with a plan?" << endl;
	else if(mafia_win())
		cout << "Losoe!" << endl;
	else
		cout << "Victory!" << endl;
}

void Room::apply_all_night_votes(){
	for(int i=0; i<players.size(); i++) {
			if(players[i]->made_vote())
				players[i]->apply_vote();
	}
}

int Room::max_incoming_vote_index(){
	int max = 0;
	int index;
	for(int i=0; i<players.size(); i++) {
		int incoming_votes = players[i]->get_incoming_votes();
		if( max < incoming_votes || ( max == incoming_votes && rand()%2) ){
			max = incoming_votes;
			index = i;
		}
	}
	return index;
}

void Room::show_kill_trying(Player* target) { cout << "Mafia try to kill " << target->get_username() << endl; }

void Room::show_kill_result(Player* target) { cout << "Killed " << target->get_username() << endl; }

int Room::kill_night_target(){
	if(!players[night_target_index]->get_healed_status())
		players[night_target_index]->die(true);
	
	if(!players[night_target_index]->get_alive_status()){

		switch(players[night_target_index]->get_team()){
			case Player::VILLAGER:
			case Player::SINGLE_JOKER:
				number_of_villagers--;
				break;
			case Player::MAFIA:
				number_of_mafias--;
				break;
		}
		return night_target_index;
	}
	else return (-1);
}

void Room::select_night_target(){
	apply_all_night_votes();
	night_target_index = max_incoming_vote_index();
	show_kill_trying(players[night_target_index]);
}

void Room::show_die_result(Player* target) { cout << "Died " << target->get_username() << endl; }

void Room::kill_day_target(){
	int target_index = max_incoming_vote_index();
	players[target_index]->die(false);

	switch(players[target_index]->get_team()){
		case Player::SINGLE_JOKER:
			joker_win = true;
			break;
		case Player::VILLAGER:
			number_of_villagers--;
			break;
		case Player::MAFIA:
			number_of_mafias--;
			break;
	}

	show_die_result(players[target_index]);
}

void Room::show_day_flag(){ cout << "Day " << day_number << endl; }

void Room::end_night(){
	night_time = false;
	day_number++;
	int night_dead = kill_night_target();
	night_target_index = -1;

	recovery_all();
	show_day_flag();
	if(night_dead != -1) show_kill_result(players[night_dead]);
	show_silences_result();
}

void Room::show_silences_result() {
	if(night_silenced_indexs.size() <= 0) return;

	sort_indexes();

	cout << "Silenced";
	while( night_silenced_indexs.size() > 0 ) {
		cout << " " << players[ night_silenced_indexs[night_silenced_indexs.size()-1] ]->get_username();
		night_silenced_indexs.pop_back();
	}
	cout << endl;
}

void Room::sort_indexes() {
	for(int i=0; i<night_silenced_indexs.size(); i++)
		for(int j=i+1; j<night_silenced_indexs.size(); j++)
			if(players[night_silenced_indexs[i]]->get_username() < players[night_silenced_indexs[j]]->get_username()) {
				int t = night_silenced_indexs[i];
				night_silenced_indexs[i] = night_silenced_indexs[j];
				night_silenced_indexs[j] = t;
			}
}

void Room::recovery_all() {
	for(int i=0; i<players.size(); i++)
		players[i]->recovery();
}

void Room::show_night_flag(){ cout << "Night " << day_number << endl; }

void Room::end_day(){
	night_time = true;
	apply_all_day_votes();
	kill_day_target();
	recovery_all();
	recovery_all_silenced();
	show_night_flag();
}

void Room::recovery_all_silenced() {
	for(int i=0; i<players.size(); i++)
		players[i]->silencing_recovery();
}

void Room::apply_all_day_votes(){
	for(int i=0; i<players.size(); i++) {
			if(players[i]->made_vote()){
				players[i]->apply_vote();
				players[i]->clean_vote();
			}
	}
}

void Room::vote(string voter, string votee) {
	int voter_index = find_player_index(voter);
	int votee_index = find_player_index(votee);

	if(voter_index<0 || votee_index<0) throw new _UserNotJoined_();

	if(!is_this_index_alive(voter_index)) throw new _PersonIsDead_();
	if(!is_this_index_alive(votee_index)) throw new _UserAlreadyDied_();

	players[voter_index]->make_vote(players[votee_index], night_time);

	if(night_time && command_need()!="Vote") select_night_target();
}

void Room::detect(string detective, string suspect) {
	int detective_index = find_player_index(detective);
	int suspect_index = find_player_index(suspect);

	if(detective_index<0 || suspect_index<0) throw new _UserNotJoined_();

	if(!is_this_index_alive(detective_index) || !is_this_index_alive(suspect_index)) throw new _PersonIsDead_();

	bool OK = players[detective_index]->detect(players[suspect_index]);
	if(OK) cout << "Yes" << endl;
	else std:cout << "No" << endl;
}

void Room::heal(string doctor, string person) {
	int doctor_index = find_player_index(doctor);
	int person_index = find_player_index(person);

	if(doctor_index<0 || person_index<0) throw new _UserNotJoined_();

	if(!is_this_index_alive(doctor_index) || !is_this_index_alive(person_index)) throw new _PersonIsDead_();

	players[doctor_index]->save(players[person_index]);
}

void Room::silent(string silencer, string person) {
	int silencer_index = find_player_index(silencer);
	int person_index = find_player_index(person);

	if(silencer_index<0 || person_index<0) throw new _UserNotJoined_();

	if(!is_this_index_alive(silencer_index) || !is_this_index_alive(person_index)) throw new _PersonIsDead_();

	players[silencer_index]->make_silence(players[person_index]);

	for(int i=0; i<night_silenced_indexs.size(); i++)
		if(night_silenced_indexs[i] == person_index)
			return;
	
	night_silenced_indexs.push_back(person_index);
}

int Room::find_player_index(string player_name) {
	for(int i=0; i<players.size(); i++)
		if(players[i]->same_name(player_name)) return i;
	return -1;
}

bool Room::is_this_index_alive(int index) { return players[index]->get_alive_status(); }

bool Room::next_time() {
	if(night_time) end_night();
	else end_day();
	if(end_game()) {
		show_game_result();
		return false;
	}
	return true;
}

string Room::command_need() {
	if(!enough_player())
		return "Join";
	
	else if(night_time) {
		if(need_night_vote())
				return "Vote";
		if(need_night_detect())
				return "Detect";
		if(need_night_heal())
				return "Heal";
		if(need_night_silent())		
				return "Silent";
		else return "end_night";
	}
	else
		return "Vote";
}

bool Room::need_night_heal(){
	for(int i=0; i<players.size(); i++)
			if(players[i]->get_role() == Player::DOCTOR
			&& players[i]->get_alive_status()
			&& !players[i]->get_this_night_healed()		)
				return true;
	return false;
}

bool Room::need_night_silent(){
	for(int i=0; i<players.size(); i++)
			if(players[i]->get_role() == Player::SILENCER
			&& players[i]->get_alive_status()
			&& !players[i]->get_this_night_silence()	)
				return true;
	return false;
}

bool Room::need_night_detect(){
	for(int i=0; i<players.size(); i++)
			if(players[i]->get_role() == Player::DETECTIVE
			&& players[i]->get_alive_status()
			&& !players[i]->get_this_night_detected()	)
				return true;
	return false;
}

bool Room::need_night_vote(){
	for(int i=0; i<players.size(); i++)
			if(players[i]->get_team() == Player::MAFIA
			&& players[i]->get_alive_status()
			&& !players[i]->made_vote()		)
				return true;
	return false;
}

bool Room::mafia_win() { return number_of_mafias >= number_of_villagers; }

bool Room::villager_win() { return number_of_mafias == 0; }

bool Room::get_night_time() { return night_time; }

void Room::list_players() {
	if(!ready_to_show_list) return;

	for(int i=0; i<players.size(); i++)
		cout << players[i]->get_username() << " is "
			<< string_form_of_role(players[i]->get_role()) << endl;
	show_day_flag();
}

string Room::string_form_of_role(Player::Role role) {
	switch(role){
		case Player::JOKER:
			return "Joker";
		case Player::SIMPLE_VILLAGER:
			return "Villager";
		case Player::SIMPLE_MAFIA:
			return "Mafia";
		case Player::DETECTIVE:
			return "Detective";
		case Player::DOCTOR:
			return "Doctor";
		case Player::ROUINTAN:
			return "RouinTan";
		case Player::GODFATHER:
			return "GodFather";
		case Player::SILENCER:
			return "Silencer";
	}
}

void Room::print_state() {
	cout << "Mafia = " << number_of_mafias << endl;
	cout << "Villager = " << number_of_villagers << endl;
}