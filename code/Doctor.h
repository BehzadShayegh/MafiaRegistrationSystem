#ifndef DOCTOR_H
#define DOCTOR_H

#include "Villager.h"

class Doctor : public Villager {

public:
	Doctor(std::string);
	~Doctor();
	
	void save(Player*);
	void recovery();

	bool get_this_night_healed();
	Player::Role get_role();
private:
	bool this_night_healed;

};

#endif