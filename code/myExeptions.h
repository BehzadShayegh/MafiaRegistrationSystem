#ifndef MYEXEPTIONS_h
#define MYEXEPTIONS_h

#include <iostream>

class My_Exeptions {
public:
	virtual void print_error() = 0;
};

class UnknownCommand : public My_Exeptions {
public:
	void print_error();
};

class UnExpectedCommand : public My_Exeptions {
public:
	void print_error();
};

class RepetitiveRoom : public My_Exeptions {
public:
	void print_error();
};

class MoreMafiaRolesThanNumberOfMafias : public My_Exeptions {
public:
	void print_error();
};

class MoreVillagerRolesThanNumberOfVillagers : public My_Exeptions {
public:
	void print_error();
};

class UnknownRole : public My_Exeptions {
public:
	void print_error();
};

class _InvalidRoomName_ : public My_Exeptions {
public:
	void print_error();
};

class RepetitivePlayer : public My_Exeptions {
public:
	void print_error();
};

class _ManyUsers_ : public My_Exeptions {
public:
	void print_error();
};

class _UserNotJoined_ : public My_Exeptions {
public:
	void print_error();
};

class _ThisUserHasBeenSilencedBefor_ : public My_Exeptions {
public:
	void print_error();
};

class _UserAlreadyDied_ : public My_Exeptions {
public:
	void print_error();
};

class _ThisUserCanNotVote_ : public My_Exeptions {
public:
	void print_error();
};

class _DetectiveHasAlreadyAsked_ : public My_Exeptions {
public:
	void print_error();
};

class _UserCanNotAsk_ : public My_Exeptions {
public:
	void print_error();
};

class _PersonIsDead_ : public My_Exeptions {
public:
	void print_error();
};

class _DoctorHasAlreadyHealed_ : public My_Exeptions {
public:
	void print_error();
};

class _UserCanNotHeal_ : public My_Exeptions {
public:
	void print_error();
};

class _SilencerHasAlreadySilenced_ : public My_Exeptions {
public:
	void print_error();
};

class _UserCanNotSilence_ : public My_Exeptions {
public:
	void print_error();
};

class NeedNumberOfRole : public My_Exeptions {
public:
	void print_error();
};

class NumberOfRoleCanntBeLessThan0 : public My_Exeptions {
public:
	void print_error();
};

class RoomNeedName : public My_Exeptions {
public:
	void print_error();
};

class NeedVoterAndVotee : public My_Exeptions {
public:
	void print_error();
};

class NeedDetectiveAndSuspect : public My_Exeptions {
public:
	void print_error();
};

class NeedDoctorAndPerson : public My_Exeptions {
public:
	void print_error();
};

class NeedSilencerAndPerson : public My_Exeptions {
public:
	void print_error();
};

class ChooseARoom : public My_Exeptions {
public:
	void print_error();
};

#endif